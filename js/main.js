
// https://docs.djangoproject.com/en/2.2/ref/csrf/#ajax
// do proper POSTs to buy things requires csrf, essentially copypasted from the django docu linked above
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function getCsrftoken() {return getCookie('csrftoken'); }

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", getCsrftoken());
        }
    }
});

function addAlert(message, type) {
$('#alerts').prepend(
'<div class="alert alert-' + type + ' alert-dismissable">'+
    '<button type="button" class="close" ' +
    'data-dismiss="alert" aria-hidden="true">' +
    '&times;' +
    '</button>' +
    message +
'</div>');
$(".alert-dismissable").fadeTo(8000, 500).slideUp(500, function(){
    $(".alert-dismissable").alert('close');
});
};

function buyItem(user, itemSlug) {
// e.g. http://localhost:8000/api/buy/item/3/club-mate-05/
    $.post( '/api/buy/item/' + user + '/' + itemSlug + '/', {} ,   function( data ) {
    addAlert(data.item + ' gekauft. Neues Guthaben von <b>' + data.user + '</b>: ' + data.balance + 'Euro.', 'success');
    //TODO: Check who buyed stuff and convert . to ,
    //$('#balance_value').text(data.balance);
    });
}

function buyItemWithPin() {
// e.g. http://localhost:8000/api/buy/item/3/club-mate-05/
user = $('#pinUser').val();
itemSlug = $('#pinItemSlug').val();
itemId = $('#pinItemId').val();
pin = $('#pinPin').val();
$.post( '/api/buy/item/' + user + '/' + itemSlug + '/', {pin: pin},function( data ) {
    addAlert(data.item + ' mit Pin gekauft. Neues Guthaben von <b>' + data.user + '</b>: ' + data.balance + 'Euro.', 'success');
    }).fail(function() {
    addAlert('Kauf fehlgeschlagen. Pin korrekt?', 'danger');
    });
    //close modals, reset pin field
    $('#pinPin').val('');
    $('#pinModal').modal('hide');
    $('#buy-' + itemId).modal('hide');
}

function buyItemBtn(user, itemSlug, itemId) {
buyItem(user, itemSlug);
$('#buy-' + itemId).modal('hide');
}

function cashbuyItemBtn(itemSlug, itemId) {
$.post( '/api/buy/item/' + itemSlug + '/cash/', {}, function( data ) {
    addAlert(data.item + ' bar gekauft.', 'success');
});
$('#buy-' + itemId).modal('hide');
}


function buyItemWithPinBtn1(user, itemSlug, itemId) {
$('#pinUser').val(user);
$('#pinItemSlug').val(itemSlug);
$('#pinItemId').val(itemId);
$('#pinModal').modal('show');
$('#pinPin').focus();
}

function buyFilamentModal(itemPrice, itemSlug, itemId, itemName) {
document.getElementById("filamentName").textContent=itemName;
$('#filaPrice').val(itemPrice);
$('#filaSlug').val(itemSlug);
$('#buy-filament').modal('show');
}

function buyFilament() {
var itemAmount = document.getElementById("weightField");
var itemSlug = document.getElementById("filaSlug");
$.post( '/api/buy/item/' + itemSlug.value + '/' + itemAmount.value +'/cash/', {}, function( data ) {
    addAlert('Danke für deine Spende', 'success');
});
$('#buy-filament').modal('hide');
}

function addFunds(user, amount) {
//api/accounts/1/add/funds/10/
$.post( '/api/accounts/' + user + '/add/funds/' + amount + '/', function( data ) {
addAlert('Guthaben mit ' + amount + '€ aufgeladen. Aktuelles Guthaben: ' + data.balance + '€', 'success');
});
}

